package org.SIDIBE.Serie5.Exo12;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashMap;
import java.util.ArrayList;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

public class Exercice12 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		List<String> strings = Arrays.asList("one", "two", "three", "four", "five", "six", "seven", "eight", "nine",
				"ten", "eleven", "twelve");
		List<String> copyStrings = new ArrayList<>(strings);
		System.out.println("Question1 :voici le contenu de la liste:");
		strings.forEach(s -> System.out.println(s));

		copyStrings.removeIf(s -> s.length() > 3);

		System.out.println("Question2 : voici le contenu de la liste modifie:");
		 copyStrings.forEach(s -> System.out.println(s));

		Comparator<String> plusLongChaine = Comparator.comparingInt(s -> s.length());
		strings.sort(plusLongChaine);
		System.out.println("Question 3 :voici le contenu de la liste tri�:");
		 strings.forEach(s -> System.out.println(s));
		 //**********************question 4**********************
		Map<Integer, List<String>> tableHachageT = new HashMap<Integer, List<String>>();

		ListIterator<String> it = strings.listIterator();

		while (it.hasNext()) {
			String element = it.next();
			int cle = element.length();
			tableHachageT.computeIfAbsent(cle, key -> new ArrayList<>()).add(element);
		}
		System.out.println("Question 4 :");
		tableHachageT.forEach((taille, list) -> System.out.println(taille + "->" + list));
		//***********************question 5**********************
		Map<String, List<String>> tableHachageT1 = new HashMap<String, List<String>>();

		ListIterator<String> it1 = strings.listIterator();

		while (it1.hasNext()) {
			String element = it1.next();
			String cle = element.substring(0, 1);
			tableHachageT1.computeIfAbsent(cle, key -> new ArrayList<>()).add(element);
		}
		System.out.println("Question 5 :");
		tableHachageT1.forEach((c, list) -> System.out.println(c + "->" + list));
		
        //*******************question 6****************************
		Map<String, Map<Integer, List<String>>> tableHachageT2 = new HashMap<String, Map<Integer, List<String>>>();
		ListIterator<String> it2 = strings.listIterator();
		while (it2.hasNext()) {
			String element = it2.next();
			int cle = element.length();
			String cleS = element.substring(0, 1);
			tableHachageT2.computeIfAbsent(cleS, key -> new HashMap<>()).computeIfAbsent(cle, key -> new ArrayList<>())
					.add(element);
		}
		System.out.println("Question 6 :");
		tableHachageT2.forEach((c, m) -> System.out.println(c + "->" + m));

		//*********************question 7*******************************
		Map<Integer, String> tableHachageT3 = new HashMap<Integer, String>();
		ListIterator<String> it3 = strings.listIterator();
		while (it3.hasNext()) {
			String element = it3.next();
			int cle = element.length();
		
			tableHachageT3.merge(cle, element.concat(","),String::concat );
			
		}
		System.out.println("Question 7 : "); 
		tableHachageT3.forEach((c, m) -> System.out.println(c + "->" + m));
		
	}

}

